# Appendix: Other Sources of Information
*Alan Coopersmith*

  * X.Org website:	http://www.x.org/wiki/

  * Documentation from latest X.Org release:  http://www.x.org/releases/current/doc/

  * Blogs from X.Org & freedesktop.org developers:  http://planet.x.org/

  * Other sites full of information:
    * Wikipedia X overview: http://en.wikipedia.org/wiki/X_Window_System
    * Kenton Lee's X pages: http://www.rahul.net/kenton/index.shtml
    * Wikibooks Guide to X11: http://en.wikibooks.org/wiki/Guide_to_X11

  * The awful truth revealed: http://www.rahul.net/kenton/xvirus.html

  * Success defined: http://xkcd.com/963/
