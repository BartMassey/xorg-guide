# Contributing To X
*Matt Dew*

This section describes skills new developers will need to
successfully contribute to X. As noted in the Concepts
chapter, there is a lot to X.org and there is a significant
learning curve.  However, the curve is less steep than you
might think, and the rewards are worth it.

One thing that bears repeating is that X is a fairly
complicated project.  It is one of those projects where you
will not understand everything---nor are you expected to.
There is simply too much. Nobody on the project understands
everything, not even the graybeards.  However, you will be
able to understand parts of the system immediately, and most
of it eventually.  Most X.org developers are truly nice and
genuinely want to help you when you have questions.

If you are uncomfortable with the tools used to develop X,
you are going to have a difficult time.  This admonition may
seem harsh, but it is not meant to discourage you.  Rather
it is meant to remind you to prepare up front, so that you
are properly equipped.

Here is a list of things you will need to understand. Maybe
they are obvious, but you might be surprised how many people
jump in without them.

  1. The C programming language. If you are relatively
  unfamiliar with C, X may not be the right project for you.
  You do not need to be a C guru, but you need to be
  comfortable working with the language.  If you are working
  on part of the project that is not directly written in C
  (for example, the XCB language binding generator or Python
  bindings), you might be OK. However, you will never be far
  from a code base written in C.  If you cannot program,
  there are still plenty of ways for you to contribute to
  X. Talk to the documentation or build system folks: they
  can always use help.

  2. Revision control / version control / source code
  management. You need to be comfortable with the concepts
  behind modern source code management systems (call them
  what you will). X.org uses Git for most of its work, so
  you will need to learn at least the basics of Git. Git is
  a distributed source code management system rather than a
  centralized one. The more familiar you are with Git the
  better off you are; however, you can learn simple Git
  commands quickly if you understand source code management
  and why it is important.

  3. Building and compiling programs from scratch in the
  UNIX environment. X is a software project. That means you
  will be compiling software.  Really, really cool software,
  but still software.

  4. Working remotely and asynchronously. X developers are
  scattered around the world.  Communication among them
  involves the use of IRC, email lists, Skype, IM and other
  high-tech tools.  Rarely are you in the same room, or even
  the same time zone, as a developer you are trying to work
  with. This can be a bit frustrating: sometimes questions
  are hard to ask, and sometimes replies to your questions
  are slow.

  5. Debugging. Debugging is an integral part of large
  projects like X. You should be familiar with standard
  debugging tools such as gdb, valgrind and strace. More
  importantly, you should know the methodology of debugging,
  with or without tools.  Luckily, you need not be
  extraordinarily proficient: there are plenty of smart
  developers who can help you.
  
  6. Testing. You need not be a testing guru. However, you
  should be comfortable with creating and running basic
  tests of your code. You should know what a regression test
  is, and be prepared to use one. You should be unsurprised
  when X developers refuse to commit your untested patches.

If you are comfortable in these areas, you should be in
good shape.  Again, you don't need to be guru-level. You
need to be familiar enough with the items on this list that
you can focus on design and implementation rather than
getting stuck.

The other chapters of this book discuss the overall
architecture and individual parts of X. Here's a quick
summary of the technologies X.Org uses:

  * C
  * Multiple Git repositories
  * Bugzilla
  * IRC channels
  * Mailing lists
  * Doxygen
  * DocBook/XML (not SGML)
  * MoinMoin (wiki)


Below is a list of resources that you can refer to as you
begin to work on X.

  * IRC - irc.freenode.net  channels: #dri, #dri-devel, #wayland, #xcb, #xorg, #xorg-devel, #nouveau
  * Email lists:
    * http://lists.x.org/archives/xorg-devel/
    * http://lists.x.org/archives/xorg/
    * http://lists.freedesktop.org/archives/xcb/
    * http://dri.freedesktop.org/wiki/MailingLists
  * Bugzilla - https://bugs.freedesktop.org
  * Git - http://cgit.freedesktop.org/
  * Wiki - http://www.x.org/wiki
  * Maintainers (who can help with what) - http://cgit.freedesktop.org/xorg/doc/xorg-docs/tree/MAINTAINERS

This document is still a work in progress. If you have suggestions on things that would be helpful, please let us know.

Welcome aboard.
