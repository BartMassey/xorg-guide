MD = \
  preface.md \
  concepts.md \
  communication.md \
  extensions.md \
  client-ecosystem.md \
  xlib-and-xcb.md \
  using-extensions.md \
  hutterer-kbd.md \
  fonts.md \
  debugging.md \
  contributing.md \
  other-info.md

SVG = xorg.svg stairstep.svg

COVER = front-cover.jpg

.SUFFIXES: .dot .svg


.dot.svg:
	dot -T svg $*.dot >$*.svg

guide.epub: $(MD) $(SVG) $(COVER) Makefile title.txt
	pandoc -t epub2 --epub-cover-image $(COVER) \
	    -o guide.epub title.txt $(MD)

