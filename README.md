# The X.Org New Developer's Guide
Bart Massey 2022

This work is the product of a documentation sprint sometime
in March 2012. Alan Coopersmith, Matt Dew and others
including me got together online to build a guide for new
X.Org developers.

This has sat for a long time without being touched,
sadly. It is arguably now quite stale.

The current version is built using the
[EPUB mode](https://pandoc.org/epub.html) of
[Pandoc](https://pandoc.org) and validated using W3C
[`epubcheck`](https://github.com/w3c/epubcheck).

This work is made available under the "Creative Commons
Non-Commercial Share Alike 3.0" license. See the file
`LICENSE.txt` in this distribution for license terms.
