A New Developer's Guide To X

Preface
Part - X Concepts
   - architecture diagram 
   - concept diagram
   - X is client / server
   - X in practice
     - Input
       - Keyboards
       - Mice
       - Fancy input
         - Touchpads
         - Touchscreens, tablets and multitouch
       - Image - reading from the display
     - Output
       - Rendering / Rasterization
         - Usually done w/ client side library
           or 3D HW
         - polygon rendering model
       - Graphics contexts
       - Colors (really?)
       - Sync
     - Stuff
       - Displays, Screens and Visuals
       - Grabs
       - Properties
       - Window Managers [!! --po8]
       		- ICCCM & EWMH
       - Selections, Cut-Copy-Paste
Chapter - Communication between Client and Server
   - over reliable byte streams, incl. shared memory
   - async, but ordered
     - requests
     - replies
     - events
     - errors
   - authenticated
     - magic cookie
     - xdm
Chapter - Extensions that modern developers need to know about
	- XKB
	- Xinput2
	- Composite
	- Randr
	- bigreq ? [mostly transparent to developers]
Part - X Client Developer's Guide
Chapter - The X Client Ecosystem
	- Use toolkits when possible, X libraries for things they don't provide
	- Use helper libraries like Cairo, Pango when possible, don't try to
		go straight to Xft2/render
	- Use pkg-config for CFLAGS/LDFLAGS
	- The Structure Of An X Client
Chapter - Xlib and XCB
	- Legacy Xlib stack vs. modern (but not yet complete) xcb stack
	- mixing and matching
		- xdpyinfo example from https://blogs.oracle.com/alanc/entry/porting_xwininfo_to_xcb
Chapter - Extensions from the client side
	- How they talk/work with the server (or something )
        - registration (initialization?)
	- check availability/version before using
Chapter - debugging client interactions with X server
	- xrestop / X-Resource extension
	- xscope, xmon, wireshark?
	- dtrace/systemtap
Part - X Window System Developer's Guide
Chapter - Building X Infrastructure
	- stack build tools: jhbuild, build.sh, etc.
Chapter - Server Internals Basics
	- DIX, MI, DDX
        - Event queue
        - Privates
Chapter - 2D Drivers in X
	- Initializing an X driver
	- ShadowFB
	- EXA, UXA
        - pixman
Chapter - Video Acceleration
	- video decoding stages
	- Xv
	- XvMC
	- vaapi
	- vdpau
Chapter - testing & debugging the server side
	- xts
	- xorg-gtest?
	- grab debug hot keys
Part - Modern 3D For X Developers
Chapter - Introduction to DRI
Chapter - Mesa
Chapter - Gallium
Chapter - 3D drivers in mesa
Chapter - 3D testing (piglit & friends) (??)
Part - Miscellaneous
Chapter - and then there was Wayland
Chapter - FB drivers
Chapter - Fonts
Chapter - Documentation & doc tools - how to help Matt make our docs shine
	http://www.x.org/wiki/Development/Documentation/WritingDocumentation
Chapter - How To Contribute (and the firehosing therein)
        - bug reports
        - X.Org Foundation & community (irc, mailing lists, members/board, conferences, etc.)
        - tinderbox